// Rust implementation of easter.c
// (c) torvic9

use std::env;

fn calceast(year: &u16) -> (u16, &str) {
    let a = year % 19;
    let b = year / 100;
    let c = year % 100;
    let d = b >> 2;
    let e = b % 4;

    let g = ((b << 3) + 13) / 25;
    let h = (19 * a + b - d - g + 15) % 30;
    let i = c >> 2;
    let k = c % 4;
    let l = (32 + (e << 1) + (i << 1) - h - k) % 7;
    let m = (a + 11 * h + 19) / 433;

    let month = (h + l - 7 * m + 90) / 25;
    let day = (h + l - 7 * m + 33 * month + 19) % 32;
    
    if month == 3 {
        (day, "March")
    } else {
        (day, "April")
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please provide a year as a single argument, aborting!");
    }

    let yr = &args[1];
    let yr: u16 = yr.trim().parse().expect("Fail: Please provide a year!");

    let (day, month) = calceast(&yr);

    println!("Easter is on {} of {}.", day, month);
}
