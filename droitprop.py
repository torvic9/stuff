#!/usr/bin/python
# calcul du droit proportionnel

droit_prop = 0.0

tranches = [(0.01,1735.25), (1735.26,3470.51), (3470.52,6445.23), (6445.24,14873.61), (14873.62,24789.35), (24789.36,float('inf'))]

pourc = [0.03, 0.02, 0.01, 0.005, 0.0025, 0.001]

montant = float(input("Entrez le montant en euros: "))
texte = "\n - Tranche de {} à {}: {} EUR"

for t in range(len(tranches)):
    low, high = tranches[t][0], tranches[t][1]
    if montant >= high:
        valeur = pourc[t] * (high - low)
        droit_prop += valeur
        print(texte.format(low, high, round(valeur,2)))
    elif montant < high:
        valeur = pourc[t] * (montant - low)
        droit_prop += valeur
        print(texte.format(low, montant, round(valeur,2)))
        break

print("\n --> Droit proportionnel:", round(droit_prop,2), "EUR\n")
