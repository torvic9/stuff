/* Simple Easter date calculator based on Spencer's algorithm
 * by torvic9
 */

#include <stdlib.h>
#include <stdio.h>

void calceast(unsigned int year, unsigned int* day, unsigned int* month)
{
	unsigned int a, b, c, d, e, g, h, i, k, l, m, n, p;

	a = year % 19;
	b = year / 100;
	c = year % 100;
	d = b >> 2;
	e = b % 4;

	g = ((b << 3) + 13) / 25;
	h = (19 * a + b - d - g + 15) % 30;
	i = c >> 2;
	k = c % 4;
	l = (32 + (e << 1) + (i << 1) - h - k) % 7;
	m = (a + 11 * h + 19) / 433;

	n = (h + l - 7 * m + 90) / 25;
	p = (h + l - 7 * m + 33 * n + 19) % 32;

	*month = n, *day = p;
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Please provide a year as a single argument, aborting.\n");
		exit(EXIT_FAILURE);
	}

	char *yr;
	char *fullmonth;
	unsigned int day, month, year;

	unsigned long conv = strtoul(argv[1], &yr, 10);
	year = (unsigned int)conv;

	/* Limit year to period from 1582 to 2500 */
	if (year < 1582 || year > 2500) {
		fprintf(stderr, "Year must be between 1582 and 2500.\n");
		exit(EXIT_FAILURE);
	}

	calceast(year, &day, &month);

	fullmonth = (month == 3) ? "March" : "April";

	printf("Easter in the year %u is on %u of %s.\n", year, day, fullmonth);
	exit(EXIT_SUCCESS);
}

